/*
** CSSE2310/CSSE7231 - Sample Server - to be commented in class 
** Accept connections on a given port, read text from connection
** turn it into upper case, send it back.
*/
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

#define MAXHOSTNAMELEN 128

int open_listen(int port)
{
    int fd;
    struct sockaddr_in serverAddr;
    int optVal;

    // Create socket (TCP IPv4)
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if(fd < 0) {
        perror("Error creating socket");
        exit(1);
    }

    // Allow address (port number) to be reused immediately
    optVal = 1;
    if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &optVal, sizeof(int)) < 0) {
        perror("Error setting socket option");
        exit(1);
    }

    // Populate server address structure to indicate local address our
    // server is going to listen on
    serverAddr.sin_family = AF_INET;	// IP v4
    serverAddr.sin_port = htons(0);	// port num
    //serverAddr.sin_addr.s_addr = htonl(0);	// any IP address of this host

    // Bind our socket to address we just created
    
    if(bind(fd, (struct sockaddr*)&serverAddr, sizeof(struct sockaddr_in)) < 0) {
        perror("Error binding socket to port");
        exit(1);
    }

    // Indicate our willingness to accept connections. SOMAXCONN (128 default) - length
    // of queue of connections waiting to be accepted
    if(listen(fd, SOMAXCONN) < 0) {
        perror("Error listening");
        exit(1);
    }
    

    getnameinfo(
    return fd;
}

char* capitalise(char* buffer, int len)
{
    int i;

    for(i=0; i<len; i++) {
        buffer[i] = (char)toupper((int)buffer[i]);
    }
    return buffer;
}

void process_connections(int fdServer)
{
    int fd;
    struct sockaddr_in fromAddr;
    socklen_t fromAddrSize;
    int error;
    char hostname[MAXHOSTNAMELEN];
    char buffer[1024];
    ssize_t numBytesRead;

    // Repeatedly accept connections and process data (capitalise)
    while(1) {
        fromAddrSize = sizeof(struct sockaddr_in);
	// Block, waiting for a new connection. (fromAddr will be populated
	// with address of client)
        fd = accept(fdServer, (struct sockaddr*)&fromAddr,  &fromAddrSize);
        if(fd < 0) {
            perror("Error accepting connection");
            exit(1);
        }
     
	// Turn our client address into a hostname and print out both the address
	// and hostname as well as the port number
        error = getnameinfo((struct sockaddr*)&fromAddr, fromAddrSize, hostname,
                MAXHOSTNAMELEN, NULL, 0, 0);
        if(error) {
            fprintf(stderr, "Error getting hostname: %s\n", 
                    gai_strerror(error));
        } else {
            printf("Accepted connection from %s (%s), port %d\n", 
                    inet_ntoa(fromAddr.sin_addr), hostname,
                    ntohs(fromAddr.sin_port));
        }

	//Send a welcome message to our client
	write(fd,"Welcome\n", 8);

	// Repeatedly read data arriving from client - turn it to upper case - 
	// send it back to client
        while((numBytesRead = read(fd, buffer, 1024)) > 0) {
            capitalise(buffer, numBytesRead);
            write(fd, buffer, numBytesRead);
        }
	// EOF - client disconnected

        if(numBytesRead < 0) {
            perror("Error reading from socket");
            exit(1);
        }
	// Print a message to server's stdout
        printf("Done\n");
        fflush(stdout);
        close(fd);
    }
}

int main(int argc, char* argv[])
{
    int portnum;
    int fdServer;

    if(argc != 2) {
        fprintf(stderr, "Usage: %s port-num\n", argv[0]);
        exit(1);
    }

    portnum = atoi(argv[1]);
    if (portnum < 1024 || portnum > 65535) {
        fprintf(stderr,  "Invalid port number: %s\n", argv[1]);
        exit(1);
    }

    fdServer = open_listen(portnum);

    process_connections(fdServer);
    return 0;
}
