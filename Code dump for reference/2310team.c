#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#define MAXHOSTNAMELEN 128
#define MAXNARRATIVELINE 200
#include "readingsin.h"

/* set up networking for launch mode*/
int launch_wait(void) {
    int fd;
    struct sockaddr_in serverAddr;

    fd = socket(AF_INET, SOCK_STREAM, 0);

    serverAddr.sin_family = AF_INET;    // IP v4
    serverAddr.sin_port = 0;  // port num
    serverAddr.sin_addr.s_addr = INADDR_ANY;     // any IP address of this host

    if(bind(fd, (struct sockaddr*)&serverAddr, 
	    sizeof(struct sockaddr_in)) < 0) {
        perror("Error binding socket to port");
        exit(1);
    }
    socklen_t len = sizeof(struct sockaddr_in);
    getsockname(fd, (struct sockaddr*)&serverAddr, &len);
    printf("%d\n", ntohs(serverAddr.sin_port));
    fflush(stdout);
    if(listen(fd, SOMAXCONN) < 0) {
        perror("Error listening");
        exit(1);
    }
    fd = accept(fd, (struct sockaddr*) &serverAddr, &len);
    return fd;
}

/*func to set up address struct so challenge conncets to specific port */
struct in_addr* name_to_IP_addr(char* hostname)
{
    int error;
    struct addrinfo* addressInfo;

    error = getaddrinfo(hostname, NULL, NULL, &addressInfo);
    if(error) {
	return NULL;
    }
    return &(((struct sockaddr_in*)(addressInfo->ai_addr))->sin_addr);
}

/*launch the networking for challenger*/
int launch_challenge(struct in_addr* ipAddress, int port)
{
    struct sockaddr_in socketAddr;
    int fd;
    
    // Create socket - TCP socket
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if(fd < 0) {
	perror("Error creating socket");
	exit(1);
    }

    socketAddr.sin_family = AF_INET;
    socketAddr.sin_port = htons(port);
    socketAddr.sin_addr.s_addr = ipAddress->s_addr;

    // Attempt to connect to server
    if(connect(fd, (struct sockaddr*)&socketAddr, sizeof(socketAddr)) < 0) {
	exit(show_team_err(TEAMCON));
    }
    return fd;
}

/*get iselectyou message*/
void get_select_message(Game* game, FILE** comms, 
	Sinister* sinister, Team* team) {
    //recieve I choose you
    char* line = read_line(comms[0]);
    if (line == NULL) {
	exit(show_team_err(TEAMLOSS));
    }
    char message1 [strlen(line) +1];
    char selected [strlen(line) +1];
    int m = sscanf(line, "%s %s\n", message1, selected);
    free(line);
    //printf("message: %s\n", message1);
    if (m != 2) {
	//printf("10\n");
	exit(show_team_err(PROTOCOL));
    } else if (strcmp("iselectyou", message1)) {
	//printf("11\n");
	exit(show_team_err(PROTOCOL));
    } else if (agent_map(selected, sinister) == -1) {
	//printf("12\n");
	exit(show_team_err(PROTOCOL));
    }
    
    int otherAgent = agent_map(selected, sinister);
    game->otherAgent = otherAgent;
    game->otherAgentHealth = 10;
    
    char temp[MAXNARRATIVELINE]; 
    sprintf(temp, "%s chooses %s\n", game->otherTeamName, selected);
    //printf("%s", temp);
    game->prose = realloc(game->prose, sizeof(char) 
	    *(((int)strlen(game->prose) +(int)(strlen(temp))) +1));
    strcat(game->prose, temp);
}

/*send iselctyou message*/
void send_select_message(Game* game, FILE** comms, 
	Sinister* sinister, Team* team) {
    //send I chosse you
    game->myAttackNumber = 0;
    game->myAgent = team->agents[game->myAgentNumber];
    game->myAgentHealth = 10;
    fprintf(comms[1], "iselectyou %s\n", sinister->agents[game->myAgent]);
    fflush(comms[1]);
    char temp[MAXNARRATIVELINE];
    sprintf(temp, "%s chooses %s\n", team->teamName, 
	    sinister->agents[game->myAgent]);
    //printf("%s", temp);
    game->prose = realloc(game->prose, sizeof(char) 
	    *(((int)strlen(game->prose) +(int)(strlen(temp))) +1));
    strcat(game->prose, temp);
}

/*set up wait player*/
void wait_setup(Game* game, FILE** comms, Sinister* sinister, Team* team) {
    //recieve fight me irl
    char* line = read_line(comms[0]);
    if (line == NULL) {
	exit(show_team_err(TEAMLOSS));
    }
    char message [strlen(line) +1];
    char otherTeamName [strlen(line) +1];
    int m = sscanf(line, "%s %s\n", message, otherTeamName);
    free(line);
    if (m != 2) {
	//printf("1\n");
	exit(show_team_err(PROTOCOL));
    } else if (strcmp(message, "fightmeirl")) {
	//printf("2\n");
	exit(show_team_err(PROTOCOL));
    }
    game->otherTeamName = malloc(sizeof(char) *(strlen(otherTeamName) +1));
    strcpy(game->otherTeamName, otherTeamName);
    
    char temp[MAXNARRATIVELINE];
    sprintf(temp, "%s has a difference of opinion\n", game->otherTeamName);
    //printf("%s", temp);
    game->prose = realloc(game->prose, sizeof(char) 
	    *(((int)strlen(game->prose) +(int)(strlen(temp))) +1));
    strcat(game->prose, temp);
       

    //send have at you
    fprintf(comms[1], "haveatyou %s\n", team->teamName);
    fflush(comms[1]);
    
    //recieve I choose you
    game->otherAgentNumber = 0;
    get_select_message(game, comms, sinister, team);
    //send I chosse you
    game->myAgentNumber = 0;
    send_select_message(game, comms, sinister, team);
}

/*set up challenge player*/
void challenge_setup(Game* game, FILE** comms, Sinister* sinister, 
	Team* team) {
    //send fight me irl
    fprintf(comms[1], "fightmeirl %s\n", team->teamName);
    fflush(comms[1]);
    //recieve have at you
    char* line = read_line(comms[0]);
    if (line == NULL) {
	exit(show_team_err(TEAMLOSS));
    }
    char message [strlen(line)+1];
    char otherTeamName [strlen(line)+1];
    int m = sscanf(line, "%s %s\n", message,otherTeamName);
    free(line);
    if (m != 2) {
	//printf("3\n");
	exit(show_team_err(PROTOCOL));
    } else if (strcmp(message, "haveatyou")) {
	//printf("4\n");
	exit(show_team_err(PROTOCOL));
    }
    game->otherTeamName = malloc(sizeof(char)*(strlen(otherTeamName)+1));
    strcpy(game->otherTeamName, otherTeamName);
    
    char temp[MAXNARRATIVELINE];
    sprintf(temp, "%s has a difference of opinion\n", 
	    game->otherTeamName);
    //printf("%s", temp);
    game->prose = realloc(game->prose, sizeof(char)
	    *(((int)strlen(game->prose) + (int)(strlen(temp)))+1));
    strcat(game->prose, temp);

    //send I choose you
    game->myAgentNumber = 0;
    send_select_message(game, comms, sinister, team);
    //recieve I choose you  
    game->otherAgentNumber = 0; 
    get_select_message(game, comms, sinister, team);
}

/* recieve an attack message*/
int* get_attack_message(FILE** comms, 
	Sinister* sinister, Game* game) {
    int* attackInfo = malloc(sizeof(int)*2);
    char* line = read_line(comms[0]);
    if (line == NULL) {
        exit(show_team_err(TEAMLOSS));
    }
    //printf("Attack message recv: %s\n", line);
    char message [strlen(line)+1];
    char agent [strlen(line)+1];
    char attack [strlen(line)+1];
    int m = sscanf(line, "%s %s %s\n", message, agent, attack);
    //printf("message: %s\n", message);
    if (m != 3) {
	//printf("5\n");
        exit(show_team_err(PROTOCOL));
    } else if (strcmp(message, "attack")) {
	//printf("6\n");
	exit(show_team_err(PROTOCOL));
    } else if (agent_map(agent, sinister) == -1 
	    || attack_map(attack, sinister) == -1) {
	//printf("7\n");
	exit(show_team_err(PROTOCOL));
    }
    attackInfo[0] = agent_map(agent, sinister);
    //printf("agent_ind %d\n", attackInfo[0]);
    attackInfo[1] = attack_map(attack, sinister);
    //printf("attack_ind %d\n", attackInfo[1]);
    char temp[MAXNARRATIVELINE];
    sprintf(temp, "%s uses %s:", agent, attack);
    //printf("%s", temp);
    game->prose = realloc(game->prose, sizeof(char)
	    *(((int)strlen(game->prose) + (int)(strlen(temp))) + 1));
    strcat(game->prose, temp);
    //add to prose
    return attackInfo;
}

/* send an attack message to the comm port*/
int* send_attack_message(FILE** comms, Sinister* sinister, 
	Team* team, Game* game) {
    int* attackInfo = malloc(sizeof(int) * 2);
    char* attack = sinister->attacks[team->agentAttacks
	    [game->myAgentNumber][game->myAttackNumber]];
    char* agent = sinister->agents[team->agents[game->myAgentNumber]];
    fprintf(comms[1], "attack %s %s\n", agent, attack);
    fflush(comms[1]);
    attackInfo[0] = agent_map(agent, sinister);
    //printf("agent_ind %d\n", attackInfo[0]);
    attackInfo[1] = attack_map(attack, sinister);
    //printf("attack_ind %d\n", attackInfo[1]);
    char temp[MAXNARRATIVELINE];  
    sprintf(temp, "%s uses %s:", agent, attack);
    //printf("%s", temp);
    game->prose = realloc(game->prose, sizeof(char)
	    *(((int)strlen(game->prose) + (int)(strlen(temp))) + 1));
    strcat(game->prose, temp);
     
    //add to prose
    if (game->myAttackNumber == (team->agentAttackCount
	    [game->myAgentNumber] - 1)) {
	game->myAttackNumber = 0;
    } else {
	game->myAttackNumber++;
    }
    return attackInfo;
}

/*conseqences of incoming attack message*/
void effect_incoming_attack(Sinister* sinister,
	Team* team, Game* game, int* attackInfo, FILE** comms) {
    //printf("here1\n");
    int attackType = sinister->attackTypes[attackInfo[1]];
    int agentType = sinister->agentTypes[team->agents[game->myAgentNumber]];
    int effect = sinister->relations[attackType]
	    [agentType];
    //char* agent = sinister->agents[attackInfo[0]];
    //printf ("in effect: %d\n", effect);
    game->myAgentHealth -= effect;
    //printf ("my health: %d\n", game->myAgentHealth);
    
    char temp[MAXNARRATIVELINE]; 
    sprintf(temp, " %s", sinister->typeStrings[attackType][3 - effect]);
    //printf("%s", temp);
    game->prose = realloc(game->prose, sizeof(char)
	    *(((int)strlen(game->prose) + (int)(strlen(temp))) + 1));
    strcat(game->prose, temp);

    //add to prose 
    if (game->myAgentHealth < 1) {
	//add death clause
	if (game->myAgentNumber == 3) {
	    char temp[MAXNARRATIVELINE];
	    sprintf(temp, " - %s was eliminated.\n", 
		    sinister->agents[team->agents[game->myAgentNumber]]);
	    //printf("%s", temp);
	    game->prose = realloc(game->prose, sizeof(char)
		    *(((int)strlen(game->prose) + (int)(strlen(temp))) + 1));
	    strcat(game->prose, temp);
	     

	    char temp2[MAXNARRATIVELINE];
	    sprintf(temp2, "Team %s was eliminated.\n", team->teamName);
	    //printf("%s", temp);
	    game->prose = realloc(game->prose, sizeof(char)
		    *(((int)strlen(game->prose) + (int)(strlen(temp2))) + 1));
	    strcat(game->prose, temp2);
     
	    //add team end clause
	    //print narrative
	    for (int i = 0; i < strlen(game->prose); i++) {
		if (game->prose[i] == '_') {
		    game->prose[i] = ' ';
		}   
	    }
	    printf("%s", game->prose);
	    exit(OK);
	} else {
	    char temp[MAXNARRATIVELINE];
	    sprintf(temp, " - %s was eliminated.\n", 
		    sinister->agents[team->agents[game->myAgentNumber]]);
	    //printf("%s", temp);
	    game->prose = realloc(game->prose, sizeof(char)
		    *(((int)strlen(game->prose) + (int)(strlen(temp))) + 1));
	    strcat(game->prose, temp);
     
	    game->myAgentNumber++;
	    send_select_message(game, comms, sinister, team);
	}
    } else {
	char temp[MAXNARRATIVELINE];
	sprintf(temp, "\n");
	//printf("%s", temp);
	game->prose = realloc(game->prose, sizeof(char)
		*(((int)strlen(game->prose) + (int)(strlen(temp))) + 1));
	strcat(game->prose, temp);
    }
    free(attackInfo);
}

/*conseqences of outgoing attack message*/
void effect_outgoing_attack(Sinister* sinister, Team* team, 
	Game* game, int* attackInfo, FILE** comms) {
    //printf("here2\n");
    int attackType = sinister->attackTypes[attackInfo[1]];
    int agentType = sinister->agentTypes[game->otherAgent];
    int effect = sinister->relations[attackType]
	    [agentType];
    //char* agent = sinister->agents[attackInfo[0]];
    //printf ("out effect: %d\n", effect);
    game->otherAgentHealth -= effect;
    //printf("otjher health: %d\n", game->otherAgentHealth);
    //printf ("thier health: %d\n", game->otherAgentHealth);
    char temp[MAXNARRATIVELINE];
    sprintf(temp, " %s", sinister->typeStrings
	    [sinister->attackTypes[attackInfo[1]]][3 - effect]);
    //printf("%s", temp);
    game->prose = realloc(game->prose, sizeof(char)
	    *(((int)strlen(game->prose) + (int)(strlen(temp))) + 1));
    strcat(game->prose, temp);
    
    //add to prose
    
    if (game->otherAgentHealth < 1) {
	//add death clause
	if(game->otherAgentNumber == 3) {
	    char temp[MAXNARRATIVELINE];
	    sprintf(temp, " - %s was eliminated.\n", 
		    sinister->agents[game->otherAgent]);
	    //printf("%s", temp);
	    game->prose = realloc(game->prose, sizeof(char)
		    *(((int)strlen(game->prose) + (int)(strlen(temp))) + 1));
	    strcat(game->prose, temp);
	    
	    
	    char temp2[MAXNARRATIVELINE];
	    sprintf(temp2, "Team %s was eliminated.\n",
		    game->otherTeamName);
	    //printf("%s", temp);
	    game->prose = realloc(game->prose, sizeof(char)
		    *(((int)strlen(game->prose) + (int)(strlen(temp2))) + 1));
	    strcat(game->prose, temp2);
	    for (int i = 0; i < strlen(game->prose); i++) {
		if (game->prose[i] == '_') {
		    game->prose[i] = ' ';
		}   
	    }
	    printf("%s", game->prose);
	    //add team end clause 
	    //confirm read failure
	    //printf narrative
	    exit(OK);
	} else {
	    char temp[MAXNARRATIVELINE];
	    sprintf(temp, " - %s was eliminated.\n", 
		    sinister->agents[game->otherAgent]);
	    //printf("%s", temp);
	    game->prose = realloc(game->prose, sizeof(char)
		    *(((int)strlen(game->prose) + (int)(strlen(temp))) + 1));
	    strcat(game->prose, temp);
	    //printf("got here\n"); 
	    game->otherAgentNumber++;
	    get_select_message(game, comms, sinister, team);
	}
    } else {
	char temp[MAXNARRATIVELINE];
	sprintf(temp, "\n");
	//printf("%s", temp);
	game->prose = realloc(game->prose, sizeof(char)
		*(((int)strlen(game->prose) + (int)(strlen(temp))) + 1));
	strcat(game->prose, temp);
    }
    free(attackInfo);
}


/* play the game here, main loop*/
void play_game(Game* game, FILE** comms, 
	Sinister* sinister, Team* team, char mode) {   
    if(mode == 'c') {
	int* attackInfo = send_attack_message(comms, sinister, team, game);
	effect_outgoing_attack(sinister, team, game, attackInfo, comms);
	//attack
	    //change oppostion health
	    // check if dead
	    // add to prose
    }
    while(1) {
	int* attackInfo = get_attack_message(comms, sinister, game);
	effect_incoming_attack(sinister, team, game, attackInfo, comms);
	attackInfo = send_attack_message(comms, sinister, team, game);
	effect_outgoing_attack(sinister, team, game, attackInfo, comms);
    }
}

/* set up game if wait mode*/
Game* wait_game(FILE** comms, Sinister* sinister, Team* team) {
    Game* game = malloc(sizeof(Game));
    game->prose = malloc(sizeof(char) * 1);
    game->prose[0] = '\0'; 
    wait_setup(game, comms, sinister, team);
    play_game(game, comms, sinister, team, 'w');
    return game;
}

/*set up game in challenge mode*/
Game* challenge_game(FILE** comms, Sinister* sinister, Team* team) {
    Game* game = malloc(sizeof(Game)); 
    game->prose = malloc(sizeof(char) * 1);
    game->prose[0] = '\0'; 
    challenge_setup(game, comms, sinister, team);
    play_game(game, comms, sinister, team, 'c');
    return game;
}

int main(int argc, char** argv) {
    signal(SIGPIPE, SIG_IGN);
    if (argc == 3) {	
	// sim
	FILE* teamFile = fopen(argv[2], "r");
	if (teamFile == NULL) {
	    exit(show_team_err(TEAMOPEN));
	}
	char* err;
	int portNum = strtol(argv[4], &err, 10);
    	if((*err != '\0') || portNum < 1024 || portNum > 65535) {
            exit(show_team_err(PORT));
    	}
	//get sinsiter from controller
	//Team* team = build_team(teamFile, sinister);
	//launch sim
	
	exit(21);
    } else if (argc == 4 && !strcmp(argv[1], "wait")) {
	//wait
	FILE* sinisterFile = fopen(argv[3], "r");
	FILE* teamFile = fopen(argv[2], "r");
	if(sinisterFile == NULL) {
	    exit(show_team_err(SINOPEN));
	}
	if (teamFile == NULL) {
	    exit(show_team_err(TEAMOPEN));
	}
	 
	Sinister* sinister = build_sinister(sinisterFile);
	Team* team = build_team(teamFile, sinister);
	int fd = launch_wait();
	int fd2 = 0;
	dup2(fd, fd2);
	FILE* r = fdopen(fd, "r");
	FILE* w = fdopen(fd2, "w");
	FILE** comms = malloc(sizeof(FILE*) * 2);
	//close(fd);
	//close(fd2);
	comms[0] = r;
	comms[1] = w;
	wait_game(comms, sinister, team);
	exit(0);
	
	//wait team launch
	
    } else if (argc == 5 && !strcmp(argv[1], "challenge")) {
	//challenge
	FILE* sinisterFile = fopen(argv[3], "r");
	FILE* teamFile = fopen(argv[2], "r");
	if(sinisterFile == NULL) {
	    exit(show_team_err(SINOPEN));
	}
	if (teamFile == NULL) {
	    exit(show_team_err(TEAMOPEN));
	} 
	Sinister* sinister = build_sinister(sinisterFile);
	Team* team = build_team(teamFile, sinister);	
	char* err;
	int portNum = strtol(argv[4], &err, 10);
    	if((*err != '\0') || portNum < 1024 || portNum > 65535) {
            exit(show_team_err(PORT));
    	}
	int fd = launch_challenge
		(name_to_IP_addr("moss.labs.eait.uq.edu.au"), portNum);
	int fd2 = 0;
	dup2(fd, fd2);
	FILE* r = fdopen(fd, "r");
	FILE* w = fdopen(fd2, "w");
	FILE** comms = malloc(sizeof(FILE*) * 2);
	//close(fd);
	//close(fd2);
	comms[0] = r;
	comms[1] = w;
	challenge_game(comms, sinister, team);
	
	//challenge team launch

    } else {
	exit(show_team_err(USAGE));
    }
}
