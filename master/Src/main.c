/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */

#define LEFTFWD htim4->CCR2
#define RIGHTFWD htim4->CCR1
#define LEFTREV htim4->CCR4
#define RIGHTREV htim4->CCR3
#define LEFTCORRECTION 1
#define RIGHTCORRECTION 1
#define FORWARDSPEED 100
#define REVERSESPEED 100




/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c2;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart2;
UART_HandleTypeDef huart6;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
char current_mode;
uint32_t last_alive_time;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART6_UART_Init(void);
static void MX_TIM4_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C2_Init(void);
static void MX_ADC1_Init(void);

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
                            
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

int  check_BT_connect(void);
char get_BT_char(void);
void send_connection_message(void);

void do_start_current_mode(void);
void start_hoist_mode(void);
void start_rover_mode(void);
void start_manipulator_mode(void);
void start_leds(void);
void start_rover_hoist_mode(void);
void start_rover_slow_mode(void);
void set_manipulator_init_position(void);

void do_switching(char switchingChar);
void switch_from_hoist(char switchingChar);
void switch_from_rover(char switchingChar);
void switch_from_manipulator(char switchingChar);
void switch_from_prop(char switchingChar);
void switch_from_rover_hoist(char switchingChar);
void switch_from_rover_slow(char switchingChar);

void do_control(char controlChar);
void hoist_control(char controlChar);
void rover_control(char controlChar);
void manipulator_control(char controlChar);
void led_control(char controlChar);
void rover_hoist_control(char controlChar);
void rover_slow_control(char controlChar);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/************************************************************************/
/* Communication functions 
		These functions are used to communicate between 
		rover and the bluetooth connected control station 
		and to check the existence of this connection.
*/
/************************************************************************/
#ifdef __GNUC__
	#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else 
	#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE* f)
#endif /* __GNUC__ */

	PUTCHAR_PROTOTYPE 
{
	HAL_UART_Transmit(&huart2, (uint8_t*)&ch, 1, 0xFFFF);
	return ch;
}

/** 
	* @brief Function to get a character from control station over bluetooth
	*	@params none
	*	@retval character recieved from control station
*/
int check_alive(void) {
	if (HAL_GetTick() - last_alive_time > 600) {
		//printf("Dead\n\r");
		rover_control(' ');
	} else {
		//printf("Alive\n\r");
	}
}

/** 
	* @brief Function to get a character from control station over bluetooth
	*	@params none
	*	@retval character recieved from control station
*/
char get_BT_char(void) {
	char RxChar[11] = "";
	HAL_UART_Receive(&huart6, (uint8_t*)RxChar, 11, 50); //recieve from control
	//printf("%c\n\r",RxChar[0]); 
	return RxChar[0];
}

/** 
	* @brief Function to send Hello message to control station
	*	@params None
	*	@retval None.
*/
void send_connection_message(void) {
	char IntroMessage[50] = "Hello! Recog from Rover\n\r";
	HAL_UART_Transmit(&huart6,(uint8_t*)IntroMessage, 50, 50);
}







void suspend_tasks(void) {
	rover_control(' '); // stop rover motors
	//dont change manipulotr servo positions
	// turn off hoist servo
}



/************************************************************************/
/* Hardware Initialisation functions 
		These functions initalise the different 
		actuators used in each mode. The gateway function
		do_start_current_mode is used to acccess each function.
*/
/************************************************************************/
/** 
	* @brief Gateway to functions starting each mode. chooses fn based on current mode
	*	@params none
	*	@retval None.
*/
void do_start_current_mode(void) {
	if (current_mode  == 'h') {
		start_hoist_mode();
	} else if (current_mode == 'r') {
		start_rover_mode();
	} else if (current_mode == 'm') {
		start_manipulator_mode();
	} else if (current_mode == 'l') {
		start_rover_hoist_mode();
	}
}

/** 
	* @brief Function to start Hoist mode. Starts it at 0%DC (no sig)
	*	@params None
	*	@retval None
*/
void start_hoist_mode(void) {
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	int zero_speed_DC = 0;
	htim1.Instance->CCR1 = zero_speed_DC;
}

/** 
	* @brief Starting rover mode. Enable pins on, pwms at 0, all pwms started
	*	@params None
	*	@retval None
*/
void start_rover_mode(void){
	//set all 4 DC to 0 and start PWM's
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);

	htim4.Instance->CCR1 = 0;
	htim4.Instance->CCR2 = 0;
	htim4.Instance->CCR3 = 0;
	htim4.Instance->CCR4 = 0;
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 1);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 1);
}

	/** 
	* @brief Function to start manipulator mode. Starts it at 0%DC (no sig)
	*	@params None
	*	@retval None
*/
void start_manipulator_mode(void) {
	//set both PWM signal at 90degrees
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
	int DC1 = htim2.Instance->CCR1;
	int DC2 = htim2.Instance->CCR2;
	//int center_position_DC1 = 75;
	//int center_position_DC2 = 65;
	htim2.Instance->CCR1 = DC1;
	htim2.Instance->CCR2 = DC2;
}

	/** 
	* @brief Function to start manipulator mode. Starts it at 0%DC (no sig)
	*	@params None
	*	@retval None
*/
void set_manipulator_init_position(void) {
	//set both PWM signal at 90degrees
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
	int center_position_DC1 = 110;
	int center_position_DC2 = 90;
	htim2.Instance->CCR1 = center_position_DC1;
	htim2.Instance->CCR2 = center_position_DC2;
}


/** 
	* @brief Starting propellor mode. Enable pins on, pwms at 0, all pwms started
	*	@params None
	*	@retval None
*/
void start_leds(void){
	//set both DC to 0 and start PWM's
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	
	htim3.Instance->CCR1 = 0;
	htim3.Instance->CCR2 = 0;

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
}

/** 
	* @brief Starting rover mode. Enable pins on, pwms at 0, all pwms started
	*	@params None	
	*	@retval None
*/
void start_rover_hoist_mode(void){
	//set all 4 DC to 0 and start PWM's
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);

	htim4.Instance->CCR1 = 0;
	htim4.Instance->CCR2 = 0;
	htim4.Instance->CCR3 = 0;
	htim4.Instance->CCR4 = 0;
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 1);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 1);
	
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	int zero_speed_DC = 0;
	htim1.Instance->CCR1 = zero_speed_DC;
}

/** 
	* @brief Starting rover mode. Enable pins on, pwms at 0, all pwms started
	*	@params None	
	*	@retval None
*/
void start_rover_slow_mode(void){
	//set all 4 DC to 0 and start PWM's
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);

	htim4.Instance->CCR1 = 0;
	htim4.Instance->CCR2 = 0;
	htim4.Instance->CCR3 = 0;
	htim4.Instance->CCR4 = 0;
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 1);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 1);
}

/************************************************************************/
/* Switching functions 
		These functions are used to switch between modes,
		through the gateway functioin do_swiching()
		Each handles the suspension task of the current mode 
		and calls the start task of the next mode. 
*/
/************************************************************************/
/** 
	* @brief gateway to switching modes. selects fn based on current mode
	*	@params character signifying mode to switch to
	*	@retval None.
*/
void do_switching (char switchingChar){
	if ((current_mode == 'h')&&(switchingChar != 'h')) {
		switch_from_hoist(switchingChar);
	} else if ((current_mode == 'r') && (switchingChar != 'y')) {
		switch_from_rover(switchingChar);
	} else if ((current_mode == 'm') && (switchingChar != 'm')) {
		switch_from_manipulator(switchingChar);
	} else if ((current_mode == 'l') && (switchingChar != 'l')) {
		switch_from_rover_hoist(switchingChar);
	} else if ((current_mode == '/') && (switchingChar != '/')){
		switch_from_rover_slow(switchingChar);
	}
	printf("Switched to: %c\n\r", current_mode);
}

/** 
	* @brief Switching from hoist mode to prop or rover mode. Turns off hoist
	*	@params character signifying mode to switch to
	*	@retval None.
*/
void switch_from_hoist(char switchingChar) {
	hoist_control(' '); //turn off hoist
	if (switchingChar == 'y') {
		current_mode = 'r';
	} else if (switchingChar == 'm') {
		current_mode = 'm';
	} else if (switchingChar == 'l') {
		current_mode = 'l';
	} else if (switchingChar == '/') {
		current_mode = '/';
	}
}

/** 
	* @brief Switching from rover mode to prop, manipulator or hoist mode
	*	@params character signifying mode to switch to
	*	@retval None.
*/
void switch_from_rover(char switchingChar) {
	rover_control(' '); //turn off rover
	if (switchingChar == 'm') {
		current_mode = 'm';
	} else if (switchingChar == 'h') {
		current_mode = 'h';
	} else if (switchingChar == 'l') {
		current_mode = 'l';
	}	else if (switchingChar == '/') {
		current_mode = '/';
	}
}

/** 
	* @brief Switching from mainpulator mode to prop, manipulator or hoist mode
	*	@params character signifying mode to switch to
	*	@retval None.
*/
void switch_from_manipulator(char switchingChar) {
	manipulator_control(' '); //turn off rover
	if (switchingChar == 'h') {
		current_mode = 'h';
	} else if (switchingChar == 'y') {
		current_mode = 'r';
	} else if (switchingChar == 'l') {
		current_mode = 'l';
	} else if (switchingChar == '/') {
		current_mode = '/';
	}
 }


void switch_from_rover_hoist(char switchingChar) {
	rover_control(' ');
	hoist_control(' ');
	if (switchingChar == 'h'){
		current_mode = 'h';
	} else if (switchingChar == 'm') {
		current_mode = 'm';
	} else if (switchingChar == 'y') {
		current_mode = 'r';
	} else if (switchingChar == '/') {
		current_mode = '/';
	} 
}

void switch_from_rover_slow(char switchingChar) {
	rover_control(' ');
	if (switchingChar == 'h'){
		current_mode = 'h';
	} else if (switchingChar == 'm') {
		current_mode = 'm';
	} else if (switchingChar == 'y') {
		current_mode = 'r';
	} else if (switchingChar == 'l') {
		current_mode = 'l';
	}
}

/************************************************************************/
/* Control functions 
		Functions to control the system when in mode, accessed through
		gateway function do_control(). Each function allows the user to control 
		the actuators associtated with that mode.
*/
/************************************************************************/

/** 
	* @brief Gateway to controlling system. selects control fn based on current mode
	*	@params character signifying code to control system
	*	@retval None
*/
void do_control(char controlChar){
	if ((controlChar == 'i')||(controlChar == 'o')){
		led_control(controlChar);
	} else if (current_mode == 'h') {
		hoist_control(controlChar);
	} else if (current_mode == 'r') {
		rover_control(controlChar);
	} else if (current_mode == 'm') {
		manipulator_control(controlChar);
	} else if (current_mode == 'l') {
		rover_hoist_control(controlChar);
	} else if (current_mode == '/') {
		rover_slow_control(controlChar);
	} 
}

/** 
	* @brief Controlling hoist, inputs are w, s and SPACE
	*	@params character signifying control action
	*	@retval None.
*/
void hoist_control(char controlChar){
	if (controlChar == 'w') {
		//HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
		int rise_speed_DC = 80;
		htim1.Instance->CCR1 = rise_speed_DC;
	} else if (controlChar == 's') {
		//HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
		int drop_speed_DC = 40;
		htim1.Instance->CCR1 = drop_speed_DC;
	} else if (controlChar == ' ') {
		int zero_speed_DC = 0;
		//HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
		htim1.Instance->CCR1 = zero_speed_DC;
	} 
}

/** 
	* @brief Control of rover, inputs are w,a,s,d, SPACE(brake)
	*	@params character signifying control action
	*	@retval None
*/
void rover_control(char controlChar) {
	float speed = 100;
	float scalar = 0.95;
	if(controlChar == 'a') {	
		//both motors f
		htim4.Instance->CCR2 = speed;
		htim4.Instance->CCR1 = 0;	
		
		htim4.Instance->CCR4 = speed*scalar;
		htim4.Instance->CCR3 = 0*scalar;
	} else if (controlChar == 'w') {
		//lmotor r, rmotor f
		htim4.Instance->CCR2 = speed;
		htim4.Instance->CCR1 = 0;
		
		htim4.Instance->CCR4 = 0*scalar;
		htim4.Instance->CCR3 = speed*scalar;
	} else if (controlChar == 'd') {
		//both motors r
		htim4.Instance->CCR2 = 0;
		htim4.Instance->CCR1 = speed;
		
		htim4.Instance->CCR4 = 0*scalar;
		htim4.Instance->CCR3 = speed*scalar;
	} else if (controlChar == 's') {
		//lmotor f, rmotor r
		htim4.Instance->CCR2 = 0;
		htim4.Instance->CCR1 = speed;
		
		htim4.Instance->CCR4 = speed*scalar;
		htim4.Instance->CCR3 = 0*scalar;
	} else if (controlChar == ' ') {
		//all motors stop
		htim4.Instance->CCR2 = 0;
		htim4.Instance->CCR1 = 0;
		
		htim4.Instance->CCR4 = 0*scalar;
		htim4.Instance->CCR3 = 0*scalar;
	}
}

/** 
	* @brief Control of rover, inputs are w,a,s,d, SPACE(brake)
	*	@params character signifying control action
	*	@retval None
*/
void rover_slow_control(char controlChar) {
	float speed = 30;
	float scalar = 0.95;
	if(controlChar == 'a') {	
		//both motors f
		htim4.Instance->CCR2 = speed;
		htim4.Instance->CCR1 = 0;	
		
		htim4.Instance->CCR4 = speed*scalar;
		htim4.Instance->CCR3 = 0*scalar;
	} else if (controlChar == 'w') {
		//lmotor r, rmotor f
		htim4.Instance->CCR2 = speed;
		htim4.Instance->CCR1 = 0;
		
		htim4.Instance->CCR4 = 0*scalar;
		htim4.Instance->CCR3 = speed*scalar;
	} else if (controlChar == 'd') {
		//both motors r
		htim4.Instance->CCR2 = 0;
		htim4.Instance->CCR1 = speed;
		
		htim4.Instance->CCR4 = 0*scalar;
		htim4.Instance->CCR3 = speed*scalar;
	} else if (controlChar == 's') {
		//lmotor f, rmotor r
		htim4.Instance->CCR2 = 0;
		htim4.Instance->CCR1 = speed;
		
		htim4.Instance->CCR4 = speed*scalar;
		htim4.Instance->CCR3 = 0*scalar;
	} else if (controlChar == ' ') {
		//all motors stop
		htim4.Instance->CCR2 = 0;
		htim4.Instance->CCR1 = 0;
		
		htim4.Instance->CCR4 = 0*scalar;
		htim4.Instance->CCR3 = 0*scalar;
	}
}

/** 
	* @brief Controlling manipulator, inputs are w, s and SPACE
	*	@params character signifying control action
	*	@retval None.
*/
void manipulator_control(char controlChar){
	//servos have 0 -180 range = 5% to 10% DC, as a fraction of 1000
	int DC1 = htim2.Instance->CCR1;
	int DC2 = htim2.Instance->CCR2;
	printf("DC1: %i, DC2: %i\n", DC1, DC2);
	//DC1 = claw
	//DC2 = arm
	if (controlChar == 'w') { //close
		if (DC1 <= 110) {
			int set_pos = DC1 + 2;
			htim2.Instance->CCR1 = set_pos;	
		}
	} else if (controlChar == 's') { //open
		if (DC1 >= 40) {
			int set_pos = DC1 - 2;	
			htim2.Instance->CCR1 = set_pos;	
		}
	} else if (controlChar == 'a') { //raising
		if (DC2 <= 110) {
			int set_pos = DC2 + 2;
			htim2.Instance->CCR2 = set_pos;	
		}
	} else if (controlChar == 'd') { //lowering
		if (DC2 >= 30) {
			int set_pos = DC2 - 2;
			htim2.Instance->CCR2 = set_pos;	
		}
	} else if (controlChar == ' ') {
	}
}


/** 
	* @brief Control of prop, inputs are w,s, SPACE(brake)
	*	@params character signifying control action
	*	@retval None
*/
void led_control(char controlChar) {
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
	if(controlChar == 'i') {	
		htim3.Instance->CCR1 = 1000;
		htim3.Instance->CCR2 = 1000;
	} else if(controlChar == 'o') {	
		htim3.Instance->CCR1 = 0;
		htim3.Instance->CCR2 = 0;
	}
}

/** 
	* @brief Control of rover, inputs are w,a,s,d, SPACE(brake)
	*	@params character signifying control action
	*	@retval None
*/
void rover_hoist_control(char controlChar) {
	float speed = 100;
	float scalar = 0.95;
	if(controlChar == 'a') {	
		//both motors f
		htim4.Instance->CCR2 = speed;
		htim4.Instance->CCR1 = 0;
		
		htim4.Instance->CCR4 = speed*scalar;
		htim4.Instance->CCR3 = 0*scalar;
	} else if (controlChar == 'w') {
		//lmotor r, rmotor f
		htim4.Instance->CCR2 = speed;
		htim4.Instance->CCR1 = 0;
		
		htim4.Instance->CCR4 = 0*scalar;
		htim4.Instance->CCR3 = speed*scalar;
	} else if (controlChar == 'd') {
		//both motors r
		htim4.Instance->CCR2 = 0;
		htim4.Instance->CCR1 = speed;
		
		htim4.Instance->CCR4 = 0*scalar;
		htim4.Instance->CCR3 = speed*scalar;
	} else if (controlChar == 's') {
		//lmotor f, rmotor r
		htim4.Instance->CCR2 = 0;
		htim4.Instance->CCR1 = speed;
		
		htim4.Instance->CCR4 = speed*scalar;
		htim4.Instance->CCR3 = 0*scalar;
	} else if (controlChar == ' ') {
		//all motors stop
		htim4.Instance->CCR2 = 0;
		htim4.Instance->CCR1 = 0;
		
		htim4.Instance->CCR4 = 0*scalar;
		htim4.Instance->CCR3 = 0*scalar;
		
		int zero_speed_DC = 0;
		//HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
		htim1.Instance->CCR1 = zero_speed_DC;
		
	} else if (controlChar == '-') {
		//HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
		int rise_speed_DC = 80;
		htim1.Instance->CCR1 = rise_speed_DC;
	} else if (controlChar == '+') {
		//HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
		int drop_speed_DC = 70;
		htim1.Instance->CCR1 = drop_speed_DC;
	}
}

/* USER CODE END 0 */

int main(void)
{
  /* USER CODE BEGIN 1 */
	
	last_alive_time = 0;
	
	
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_USART6_UART_Init();
  MX_TIM4_Init();
  MX_USART2_UART_Init();
  MX_I2C2_Init();
  MX_ADC1_Init();

  /* USER CODE BEGIN 2 */
	current_mode = 'h';
	do_start_current_mode();
	send_connection_message();
	start_leds();
	set_manipulator_init_position();
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
	
		char RxChar = get_BT_char();
		
		if (RxChar != '\0') {
			last_alive_time = HAL_GetTick();
		}
		
		check_alive();
		
		if ((RxChar ==  'h')|| (RxChar == 'y') ||(RxChar == 'p') ||(RxChar == 'm') || (RxChar == 'l')|| (RxChar == '/')) {
			do_switching(RxChar);    
			do_start_current_mode();
		} else if ((RxChar == 'w') || (RxChar == 'a') || (RxChar == 's') || (RxChar == 'd') 
						 ||(RxChar == '-') ||(RxChar == '+') || (RxChar == ' ') || (RxChar == 'i') || (RxChar == 'o')){
			do_control(RxChar);
		}
  }
  /* USER CODE END 3 */
}

/** System Clock Configuration
*/
void SystemClock_Config(void){

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 84;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

}

/* I2C2 init function */
static void MX_I2C2_Init(void)
{

  hi2c2.Instance = I2C2;
  hi2c2.Init.ClockSpeed = 100000;
  hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }

}

/* TIM1 init function */
static void MX_TIM1_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 840;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 1000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }

  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_TIM_MspPostInit(&htim1);

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 840;
	
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 75;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_TIM_MspPostInit(&htim2);

}

/* TIM3 init function */
static void MX_TIM3_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 4200;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1000;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_TIM_MspPostInit(&htim3);

}

/* TIM4 init function */
static void MX_TIM4_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 420;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 100;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 50;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_TIM_MspPostInit(&htim4);

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }

}

/* USART6 init function */
static void MX_USART6_UART_Init(void)
{

  huart6.Instance = USART6;
  huart6.Init.BaudRate = 9600;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_9|GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pins : PA4 PA5 PA9 PA10 */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_9|GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PC7 */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	
	  /*Configure GPIO pin : PB12 */
  GPIO_InitStruct.Pin = GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
