import serial
import msvcrt
import time
 
port = "COM6"
baud = 9600

ser = serial.Serial(port, baud, timeout=0)

if ser.isOpen():
     print(ser.name + ' is open...')
 
while True:
     char = bytes('x'.encode())
     if msvcrt.kbhit() == True:
          char = msvcrt.getch()
     ser.write(char)
     #print(char);
     time.sleep(0.1)

